# SageMaker project

# Project Structure

```
4IBD-SAGEMAKER
|   .env
|   README.md                                       <= Read it now
|   .gitignore                                      <= Don't forget using this
|
└─── default                                        s<= Folder of Project
|   |
|   └─── elasticsearch
|   |   query.md
|
└─── docker
|   |   README.md                                   <= Readme about Docker
|   |
|   └─── docker-elk
|       |   docker-compose.yml
|       |   docker-stack.yml
|       |   .travis.yml
|       |
|       └─── elasticsearch
|       |
|       └─── logstash
|       |
|       └─── kibana
|       |
|       └─── extensions
|
└─── mlProject
|   |   README.md                                   <= Readme about machine learning project
|   |   main.py
|   |   __init__.py
|   |
|   └─── query
|   |
|   └─── src
```

<h4>Team members</h4>
<table>
    <tr>
        <th>Name</th>
        <th>Github</th>
    </tr>
    <tr>
      <td>Aargan COINTEPAS</td>
      <td><a href="https://github.com/AarganC">AarganC</a> </td>
    </tr>
    <tr>
      <td>Anthonny OLIME</td>
      <td><a href="https://github.com/Citaman">Citaman</a> </td>
    </tr>
    <tr>
      <td>Manitra RANAIVOHARISON</td>
      <td><a href="https://github.com/Harisonm">harisonm</a> </td>
    </tr>
    <tr>
      <td>Bernard VONG</td>
      <td><a href="https://github.com/BernardVong">bernardVong</a> </td>
    </tr>
</table>


<h1>Getting Started</h1>

1. Mount docker
    ```
    cd ./sources
    docker-compose build
    ```
   
2. Up Docker
    ```
    docker-compose up -d
    ```

Localhost to lautch apps : 

- kibana : http://localhost:5601
- elasticsearch : http://localhost:9200/

# Data Analysis and Visualization

The goal of this project is to process a dataset using unsupervised (or supervised) machine learning methods on the corresponding dataset.

The project should contains at least the following 4 parts:
1. Analysis of the dataset
2. Applying Machine Learning on the dataset (Python and/or AWS)
3. Visualisation of the dataset and of the extracted knowledge using MyPlotLib
4. Use Logstash to load the dataset into the ElasticSearch and plot the knowledge with Kibana   


# 1. Analysis of the dataset
1. In example: the type of data, the missing values, outliers, the correlation between variables, etc. This part should contain also the analysis of the domain application and explain the goal of the approach.
Analysis of the dataset:
order to analyse the dataset, you have to extract some statistical information from the given dataset, for
2. Applying Machine Learning :
1. Apply Python with scikit-learn to reduce the dimensions and use clustering in order to cluster the dataset.
2. Analyse clustering results and compute the Silhuette index.


3. Visualization:
You can visualize the knowledge extracted from the clustering in order to present the results i.e. scatter plots using predicted colours, decision trees,...
4. ElasticSearch and Kibana:
Load the dataset in ElasticSearch and use Kibana to extract knowledge and visualize the extracted informations.
Analyse the visualizations and conclude.

Link Of Dataset : http://lipn.univ-paris13.fr/~grozavu/PredA/dataProject/

Dataset using : 
- blood-transfusion/
- waveform/

